// ==================================================================
// Author: Alpha Diallo - dial [dot] alpha [at] gmail [dot] com
// ------------------------------------------------------------------
// A script which recursively computes the (SHA1) Merkle hash of a folder.
// See: http://en.wikipedia.org/wiki/Hash_tree
// Uses chain hashing and tail recursion to
//      avoid OutOfMemory and StackOverflow exceptions,
// Uses Array.Parallel module for speed.
// Note that we hash the relative path to each filesystem object 
//      and then chain hash with the content of the object
//      to ensure that empty files and folders are taken
//      into account when verifying the integrity of the object.
//      i.e.: Hash(obj) = Hash(Hash(obj-name) + Hash(obj-content))
//          where "+" is the concatenation operator
// ==================================================================

To run this script:
	* load it into fsi
	* > merkleHash @"Path-to-target-directory";;