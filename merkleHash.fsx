﻿// ==================================================================
// Author: Alpha Diallo - dial [dot] alpha [at] gmail [dot] com
// No copyright claimed, but attribution will be appreciated.
//      Forks, pull requests and comments are welcome.
// ------------------------------------------------------------------
// A script which recursively computes the (SHA1) Merkle hash of a folder.
// See: http://en.wikipedia.org/wiki/Hash_tree
// Uses chain hashing and tail recursion to
//      avoid OutOfMemory and StackOverflow exceptions,
// Uses Array.Parallel module for speed.
// Note that we hash the relative path to each filesystem object 
//      and then chain hash with the content of the object
//      to ensure that empty files and folders are taken
//      into account when verifying the integrity of the object.
//      i.e.: Hash(obj) = Hash(Hash(obj-name) + Hash(obj-content))
//          where "+" is the concatenation operator
// ==================================================================

open System
open System.IO                          // provides classes for file I/O 
open System.Text                        // provides Enconding.GetBytes()
open System.Security.Cryptography       // provides SHA1 crypto function
open System.Collections.Generic
open System.Linq


/// <summary>
/// Function to hash a single file.
/// Takes as input a FileInfo object, and a relative path to parent folder of the file
/// </summary>  
let hashFile (file:FileInfo) (relPath:string) =
    use fileStream = file.OpenRead()
    fileStream.Position <- 0L
    let (buffer : byte[]) = Array.zeroCreate 512    // allocate buffer array of 512 bytes for reading of file
    let sha1 = new SHA1Managed()
    let fullPath = Path.Combine(relPath, file.Name)
    let pathHash = sha1.ComputeHash((new UTF8Encoding()).GetBytes(fullPath))    // hash full (relative) filePath    
        //define recursive helper func to chainhash contents of file
    let rec loop (buff : byte[]) (hash : byte[]) numBytesReadFromStream =
        match numBytesReadFromStream with
        | 0 -> hash
        | _ ->
            let numBytesRead = fileStream.Read(buff, 0, buff.Length)
            let buffHash = sha1.ComputeHash(buff)
            let chainHash = (Array.append hash buffHash) |> sha1.ComputeHash
            loop buff chainHash numBytesRead        // tail recursive! do not fear for your stack!    
    loop buffer pathHash -1     // call helper

/// <summary>
/// Function that recursively produces the hash tree of a directory. Calls: hashFile
/// Inputs: folder (DirectoryInfo object) and relPath (string) which is the relative path of the folder
/// </summary>
let rec hashFolder (folder : DirectoryInfo) (relPath : string) =
    let sha1 = new SHA1Managed()
    let folderContent = folder.EnumerateFileSystemInfos().ToArray()
    let (|FileObj|FolderObj|) (obj : FileSystemInfo) =
        if obj.Attributes.HasFlag(FileAttributes.Directory) then FolderObj
        else FileObj
    let hashObject (obj : FileSystemInfo) = 
        match obj with
        | FileObj -> hashFile (obj :?> FileInfo) relPath
        | FolderObj -> hashFolder (obj :?> DirectoryInfo) (Path.Combine(relPath, obj.Name))
    let hashOfFolderName = sha1.ComputeHash((new UTF8Encoding()).GetBytes(relPath))     // hash folder Path
    let hashOfFolderContent =       // parallel hash of folder content
        (Array.Parallel.collect hashObject folderContent)
        |> sha1.ComputeHash
    (Array.append hashOfFolderName hashOfFolderContent) |> sha1.ComputeHash     // chain hash the two, and return    
    
/// <summary>
/// Takes an array of bytes and prints HEX equivalent
/// </summary>
let inline printHex bytes = 
    bytes
    |> Array.iteri (fun i x -> if i < (bytes.Length - 1) then printf "%X:" x; else printfn "%X" x)
    
/// call this function with the path to the DIRECTORY/FOLDER that you wish to hash.
let merkleHash (path:string) = 
    let targetDir = new DirectoryInfo(path)
    match targetDir.Exists with
    | false -> printfn "No such directory"; 1   // failure
    | true -> (hashFolder targetDir targetDir.Name) |> printHex; 0  // success
